﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupCollect : MonoBehaviour
{
    public int score;
    public string touchType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent(touchType) != null)
        {
            ScoreMgr.Instance.Score += this.score;
            Destroy(this.gameObject);
        }       
    }
}
