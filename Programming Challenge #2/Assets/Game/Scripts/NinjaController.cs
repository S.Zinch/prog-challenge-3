﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class NinjaController : MonoBehaviour
{
	public float speed = 5.0f;
	public float movementThreshold = 0.5f;
    public KeyCode attackKey;
    public GameObject fireball;

	private Vector2 inputDirection;
	private Rigidbody2D _rigidbody2D;
	private Animator _animator;
    private Vector3 lookAt;
    
    private int facing;

    private void Start()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
    {
        inputDirection.x = InputCtrl.Instance.xAxis == 0 ? Input.GetAxis("Horizontal") : InputCtrl.Instance.xAxis;
        inputDirection.y = InputCtrl.Instance.yAxis == 0 ? Input.GetAxis("Vertical") : InputCtrl.Instance.yAxis;

        if (inputDirection.magnitude > movementThreshold)
		{
			_rigidbody2D.velocity = new Vector2(inputDirection.x * speed, inputDirection.y * speed);

			// Set the input values on the animator
			_animator.SetFloat("inputX", inputDirection.x);
			_animator.SetFloat("inputY", inputDirection.y);
			_animator.SetBool("isWalking", true);
            
            if(inputDirection.x > 0)
            {
                facing = 0;
            }
            else if(inputDirection.x < 0)
            {
                facing = 180;
            }
            else if (inputDirection.y > 0)
            {
                facing = 90;
            }
            else if (inputDirection.y < 0)
            {
                facing = 270;
            }
        }
		else
		{
			_rigidbody2D.velocity = new Vector2();
			_animator.SetBool("isWalking", false);
		}

        if (Input.GetKeyDown(attackKey))
        {
            _animator.SetTrigger("Attack");
            GameObject clone = Instantiate(fireball, this.transform.position, Quaternion.Euler(Vector3.forward * facing));

        }
	}
}
