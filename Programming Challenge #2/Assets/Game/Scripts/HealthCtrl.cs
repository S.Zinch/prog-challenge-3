﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCtrl : MonoBehaviour
{
    public float health;

    [SerializeField]
    private float initHealth;
    private Vector3 spawnPos;

    // Start is called before the first frame update
    void Start()
    {
        spawnPos = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0.0f)
        {
            this.transform.position = spawnPos;
            health = initHealth;
        }
    }
}
