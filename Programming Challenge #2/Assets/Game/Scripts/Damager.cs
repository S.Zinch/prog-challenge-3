﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Damager : MonoBehaviour
{
    public float damage;
    public float animationDelay;

    private Collider2D myColl;

    // Start is called before the first frame update
    void Start()
    {
        myColl = this.gameObject.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        animationDelay -= Time.deltaTime;
        if (animationDelay < 0.0f)
        {
            myColl.enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        HealthCtrl hit = collision.gameObject.GetComponent<HealthCtrl>();
        if (hit != null)
        {
            hit.health -= damage;
        }
    }
}
