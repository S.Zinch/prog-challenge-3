﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrap : MonoBehaviour
{
    public float delay;
    public float damage;
    public string touchType;
    public GameObject firePrefab;

    private bool isTriggered;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTriggered)
        {
            timer -= Time.deltaTime;
            if(timer < 0)
            {
                ShootFire();
                isTriggered = false;
                timer = delay;
            }
        }
    }

    void ShootFire()
    {
        Instantiate(firePrefab, this.gameObject.transform);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent(touchType) != null)
        {
            isTriggered = true;
        }
    }
    
}
